#
# Be sure to run `pod lib lint FADModuleManagerPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FADModuleManagerPod'
  s.version          = '0.1.0'
  s.summary          = 'Pod que gestiona los modulos que componen FAD.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Este componente es la base de gestión para los modulos complementarios de FADSDK.
                       DESC

  s.homepage         = 'http://www.firmaautografa.com'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jcjobs' => 'jcperez@na-at.com.mx' }
  s.source           = { :git => 'https://gitlab.com/NA-AT/JCPDFAD/fadmodulemanagerpod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'


  s.platform     = :ios#, "11.0"
  s.requires_arc = true
  s.ios.deployment_target = '9.0'
  s.swift_version = '4.2'
  
  #s.exclude_files = "Classes/Exclude"
  s.source_files = 'FADModuleManagerPod', 'FADModuleManagerPod/Classes/**/*.{h,m,mm,swift}'
  s.resources = 'FADModuleManagerPod/Resources/**/*.{storyboard,xib,xcassets,ttf}'
  
  s.resource_bundles = {
      'FADModuleManagerPod' => ['FADModuleManagerPod/Resources/**/*.{plist,storyboard,xib,xcassets,ttf}'],
      #:info_plist => 'FADModuleSelfiePod/Resources/FADModuleSelfiePod-Info.plist'
  }
  
  #s.public_header_files = 'FADModuleManagerPod/Classes/**/*.h'
  
  #Dependecias del pod:
  #s.ios.vendored_frameworks = 'FADModuleManagerPod/Frameworks/*.framework'
  #s.ios.vendored_libraries = 'FADModuleManagerPod/Frameworks/*.a'
  #s.frameworks = 'UIKit', 'Foundation', 'AVFoundation', 'CoreImage', 'AVFoundation', 'CoreVideo', 'CoreGraphics', 'Accelerate', 'AssetsLibrary', 'CoreMedia'
  
  #Dependecias en pod:s
  s.dependency 'Firebase'
  s.dependency 'RealmSwift'
  s.dependency 'FADModuleSelfiePod'
  
  s.static_framework = true
  s.requires_arc = true
  
  #s.pod_target_xcconfig = {
  #   'OTHER_LDFLAGS' => '$(inherited) -ObjC'
  #}
  
  
  s.license           = {
      :type => 'Copyright',
      :text => <<-LICENSE
      El presente documento establece los términos y condiciones con los cuales NA-AT Technologies, S.A. de C.V. (“NA-AT”) y sus filiales permite el acceso y uso de su portal de Internet ('https://www.na-at.com.mx') a cualquier persona que ingrese al mismo (“Usuario”). En lo sucesivo los “Términos y Condiciones”.
      
      La visita y/o uso que haga todo Usuario del Portal implica la aceptación de los Términos y Condiciones aquí establecidos, por lo que deberá leer en su totalidad el presente documento.
      
      En el caso de que no esté de acuerdo con los Términos y Condiciones de Uso y Privacidad deberá abstenerse de acceder o utilizar el Portal.
      
      NA-AT Technologies se reserva el derecho de modificar en cualquier tiempo, total o parcialmente, los Términos y Condiciones, sin necesidad de previo aviso.
      
      Los presentes Términos y Condiciones limitan la responsabilidad de NA-AT Technologies y concede al Usuario el uso del Portal de conformidad con lo que aquí se establece, al mismo tiempo que otorga a NA-AT Technologies la propiedad y control respecto de la información que se le proporcione. En el entendido de que la información personal queda sujeta a nuestro Aviso de Privacidad, mismo que aparece publicado en el Portal.
      
      
      
      1.- USO DEL SITIO Y REESTRICCIONES
      A los Usuarios les son aplicables por el simple uso o acceso a cualquiera de las Páginas que integran el Portal de NA-AT Technologies, S.A. de C.V. ('https://www.na-at.com.mx') los términos y condiciones aquí expuestos, por lo que entenderemos que los acepta, y acuerda en obligarse a su cumplimiento.
      
      No se podrá dar un uso distinto al Sitio del señalado en el presente numeral, quedando por tanto prohibido copiar, desplegar, reenviar, reproducir, reutilizar, vender, transmitir, distribuir, bajar, otorgar licencia, modificar, publicar o usar de alguna otra manera los textos, imágenes, datos, gráficas, marcas, logotipos, distintivos y nombres comerciales, pantallas, artículos, software contenidos o utilizados en el Portal y demás materiales e información que aparezcan en el Portal (“Contenido”) para fines públicos, comerciales o para cualquier otro diverso del señalado con anterioridad.
      
      
      2.- PROPIEDAD INTELECTUAL
      Los derechos de propiedad intelectual respecto de los Servicios y Contenidos, los signos distintivos y dominios de las Páginas o el Portal, así como los derechos de uso y explotación de los mismos, incluyendo su divulgación, publicación, reproducción, distribución y transformación, son propiedad exclusiva de NA-AT Technologies y sus derechos se encuentran protegidos por la Ley de la Propiedad Industrial y su Reglamento, la Ley Federal del Derecho de Autor y su Reglamento y por los Tratados Internacionales, y cuya infracción podría derivar en responsabilidad civil y penal. En ningún caso se podrá interpretar que se otorga licencia respecto del Contenido o cualquier derecho de propiedad intelectual, ni se autoriza formar ningún vínculo o hacer referencias del Portal, a menos que se permita mediante convenio que por escrito se celebre con NA-AT Technologies.
      
      
      3.-CONFIDENCIALIDAD
      NA-AT Technologies se obliga a mantener confidencial la información que reciba del Usuario que tenga dicho carácter conforme a las disposiciones legales aplicables, en los Estados Unidos Mexicanos. Toda la información que NA-AT Technologies recabe del Usuario es tratada con absoluta confidencialidad conforme las disposiciones legales aplicables.
      
      Para conocer mayor información de la protección de sus datos personales acuda a la sección "Aviso de Privacidad"
      
      
      4.- JURISDICCIÓN Y LEY APLICABLE
      Para la interpretación, cumplimiento y ejecución de los presentes Términos y Condiciones de Uso y Privacidad, el Usuario está de acuerdo en que serán aplicables las leyes Federales de los Estados Unidos Mexicanos y competentes los tribunales de la Ciudad de México, renunciando expresamente a cualquier otro fuero o jurisdicción que pudiera corresponderles en razón de sus domicilios presentes o futuros o por cualquier otra causa.
      LICENSE
  }
  
  
end
